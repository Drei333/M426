![TBZ Logo](x_gitressourcen/tbz_logo.png)
![scrum](x_gitressourcen/scrum.png)

---

[TOC]

# m426 - Software mit agilen Methoden entwickeln

2.Lehrjahr: Q1/Q2 für AP, Q1/(Q3) für WUP

## Modulbeschreibung

[Modulidentifikation m426](M426%20Software%20mit%20agilen%20Methoden%20entwickeln.pdf)

## LBV (Vorgaben für die Leistungsbeurteilung)
1. Vortrag
   - [Vortragsbewertungsraster](Ressourcen/Vortraege/Vortragsbewertungraster.pdf)
2. Basistest
     - Lernziele
         - Vorgehensmodelle unterscheiden und aufzählen können
   	    - Was sind lineare/ nicht-lineare (iterative) Vorgehensmodelle	
         - SCRUM: 3-5-3
           - Aufgaben der Rollen?		
           - Was geschieht in den Zeremonien?		
           - Was sind Artefakte?		
         - https://scrumguides.org/
3. Scrum-Experiment mit SCRUM-Teams
   - [Beurteilungsraster](Ressourcen/Scrum_Experiment/LB_M426_V1.3_Beurteilungsraster.pdf)
   - [Kriterien Päsentation SCRUM Experiment](Ressourcen/Scrum_Experiment/Präsentation_SCRUM_Experiment.pdf)

# Lektionenplanung

## Tag 01
- Vorstellung Klasse
  - Emotional Checkin
  - Aufstellung "agile know-how"
- Vorstellung LP
- Verhaltensregeln im (Fern-) Unterricht
- Quartalsplan/ Lektionenplanung
- MS Teams -> remote & share tool

&rarr; [Details Tag 01](Lektionenplanung/Tag%2001.md)

---

## Tag 02
**Sprint 0, Woche 1**
- Ergänzung Vorgehensmodelle: linear/ iterativ
  - [Einordnung lineare/ iterative Vorgehensmodelle](https://www.scnsoft.de/blog/vorgehensmodelle-der-softwareentwicklung)

&rarr; [Details Tag 02](Lektionenplanung/Tag%2002.md)

---

## Tag 03
**Sprint 0, Woche 2**

&rarr; [Details Tag 03](Lektionenplanung/Tag%2003.md)

---

## Tag 04
**Ende Sprint 0**<br>
**Sprint 1, Woche 1**
- Ergänzung «Von Product Vision zu Userstories»
  - [Userstory splitten](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
- Ergänzung zu Refinement
  - [Refinement D.E.E.P (Roman Pichler)](https://www.romanpichler.com/blog/make-the-product-backlog-deep/)
  - [Refining the Product Backlog](https://www.romanpichler.com/blog/refining-the-product-backlog/)
  - [INVEST](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/#INVEST)



&rarr; [Details Tag 04](Lektionenplanung/Tag%2004.md)

---

## Tag 05
**Sprint 1, Woche 2**

- Basistest (ecolm.com)

&rarr; [Details Tag 05](Lektionenplanung/Tag%2005.md)

---

## Tag 06
**Sprint 2, Woche 1**

&rarr; [Details Tag 06](Lektionenplanung/Tag%2006.md)

---

## Tag 07
**Sprint 2, Woche 2**

&rarr; [Details Tag 07](Lektionenplanung/Tag%2007.md)

---

## Tag 08
**Sprint 3, Woche 1**

&rarr; [Details Tag 08](Lektionenplanung/Tag%2008.md)

---

## Tag 09
**Sprint 3, Woche 2**

&rarr; [Details Tag 09](Lektionenplanung/Tag%2009.md)

---

## Tag 10
**Klassen-Review**

&rarr; [Details Tag 10](Lektionenplanung/Tag%2010.md)

---
&copy;TBZ | 21.02.2023  