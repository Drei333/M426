# Tag 02

## Programm
1. Recap: Was ist Scrum? 
   - Gruppenarbeit SCRUM-Team: Erklären Sie den scrum flow, indem Sie die Bilder am richtigen Ort platzieren. 
   - [Timeboxed](https://timer.yodi.io/): 15 Min. 
   - Q&A im Plenum
2. Vorträge
   - Kandidat 01
   - Kandidat 02
   - Kandidat 03
3. Input: «Product Vision»/ [Product Vision Board Process](/Ressourcen/Scrum/Produktevision/Product_vision_board_process.pdf)

---
## Hands-on
**Sprint 0, Woche 1**
- Check Scrum-Teams
  - Team Zusammensetzungen
  - Projekte
- Auftrag «Product Vision»
  - Erarbeitung [«Product Vision»](/Ressourcen/Scrum/Produktevision/10_The_Product_Vision_Board.pdf) im SCRUM-Team
  - [Hinweise zum Vorgehen](/Ressourcen/Scrum/Produktevision/Ueberblilck%20product%20vision.pdf)  
  - Timeboxed: 45 Min.
  - Anschliessend [Elevator pitch](https://karrierebibel.de/elevator-pitch/)

---

&copy;TBZ | 21.02.2023 
