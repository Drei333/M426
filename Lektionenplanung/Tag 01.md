# Tag 01

## Programm
1. Vortragsthemen und Termine, siehe MS Teams
   - [Tipps für Vortraege](/Ressourcen/Vortraege/Tipps)
   - [Vortragsbewertungsraster](/Ressourcen/Vortraege/Vortragsbewertungraster.pdf)
2. Vorgehensmodelle
   - Input Lehrperson
   - Gruppenarbeit
   - [Informations-Ressourcen Vorgehensmodelle](/Ressourcen/Vorgehensmodelle_Einstieg/)
3. SCRUM
   - Video: [Einführung SCRUM](https://www.youtube.com/watch?v=ZiEcq9uvi4Y&t=1s)
   - [scrumguides.org](https://scrumguides.org/)
   - [Poster Scrumguide](/Ressourcen/Scrum/Scrum%20flow/Poster/Scrum%20Guide%20Poster%20de%202.0.pdf)
   - Diskussion
---
## Hands-on
**Kickoff: SCRUM “Experiment**
- Team-Bildung: SCRUM-Team (Optimal 4-5er Team)
- Projektwahl/ Projektbestimmung
- Bestimmung Scrum Master & Product Owner

---

&copy;TBZ | 21.02.2023 