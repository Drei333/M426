## Tag 04

## Programm
1. Vorträge
   - Kandidat 07
   - Kandidat 08

## Auftrag 1

Zeit: 30 Min.<br>
Form: SCRUM Team

**Was passiert in den Meetings?**<br>
Ihr kennt jetzt den [SCRUM Flow](../Ressourcen/Scrum/Scrum%20flow/Poster/Scrum%20Guide%20Poster%20de%202.0.pdf) und wisst, welche Zeremonien in welcher Reihenfolge durchgeführt werden.

- In welcher Reihenfolge erfolgen die untenstehenden Zeremonien?
   - [Sprintplanning](../Ressourcen/Scrum/Zeremonien/Sprintplanning.pdf)
   - [Daily Scrum](/Ressourcen/Scrum/Zeremonien/daily_scrum.pdf)
   - [Refinement](/Ressourcen/Scrum/Zeremonien/Refinement/Refinement.pdf)

- Wann finden die Refinements statt und was ist der Sinn und Zweck dieser Zeremonie?

Beantwortet die obigen Fragen und versucht die obenstehenden Zeremonien zu verstehen, indem ihr die hand-outs liest. Falls diese Erklärungen nicht ausreichen, recherchiert im Internet und fasst eure Erklärungen in eurem E-Portfolio zusammen. Gebt auch die Quellen an. 

>Sobald ihr alle Fragen beantworten könnt und mit eurer eigenen Zusammenfassung fertig seid, meldet euch bei der Lehrperson. Ich mache danach einen kleinen know-how check.


## Auftrag 2

Zeit: 30 Min.<br>
Form: SCRUM Team


1. User Story Estimating, z.B. Planning Poker. Wie geht das?   
   - [handout Planning Poker](../Ressourcen/Scrum/Zeremonien/Refinement/Planning%20Poker/Planning%20Poker.pdf)
   - [Aufwand schätzen](https://www.lise.de/blog/artikel/schaetzungen-agile-softwareentwicklung) (inkl. Video-Anleitung Planning Poker)
   - In welcher Zeremonie werden die Schätzungen bzw. Planning Poker gemacht?
2. Schätzungen:
   - Was ist der Unterschied zwischen relativen und absoluten Schätzung?
   - Weshalb macht man in SCRUM relative Schätzungen und keine absolute?   
3. Was ist DoD (Defintion of Done)?
   - Was ist der Unterschied von Abnahmekriterien einer Userstory und DoD?
   - Was ist der Sinn und Zweck einer DoD?

Recherchiert im Internet, schreibt eure Zusammenfassungen sowie die Antworten der obigen Fragen. Legt sie im E-Portfolio ab. 

>Sobald ihr alle Fragen beantworten könnt und mit eurer eigenen Zusammenfassung fertig seid, meldet euch bei der Lehrperson. Ich mache danach einen kleinen know-how check.

## Auftrag 3
Ich möchte gerne eure praktische Umsetzung im SCRUM sehen. Der Scrum Master spricht sich in seinem Scrum-Team ab und **lädt die Lehrperson zu mind. 3 Zeremonien ein.**(Outlook Einladungen verteilt bis Tag 07)
Die Lehrperson nimmt nur als Beobachter teil. Diese Beobachtung hat Relevanz für die Bewertung.
- Daily Scrum
- Sprintplanning
- Refinement
- Sprint Review
- Sprint Retro

## Auftrag 4
Ihr seid jetzt bereit und könnt jetzt hands-on mit eurer Produktentwicklung anfangen!

---
## Hands-on
**Ende Sprint 0**
- Review Sprint 0
- Retrospective Sprint 0

**Sprint 1, Woche 1**
- Start Sprint 1
  - Daily Scrum
  - Planning Sprint 1
  - Bearbeitung Sprint 1

---

&copy;TBZ | 28.02.2023