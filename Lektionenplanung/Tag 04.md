## Tag 04

## Programm
1. Vorträge
   - Kandidat 06
   - Kandidat 07
   - Kandidat 08
2. Scrum Q&A
3. Auftrag SCRUM-Teams
    - User Story Estimating, z.B. Planning Poker
        - [Planning Poker](../Ressourcen/Scrum/Zeremonien/Refinement/Planning%20Poker/Readme.md)
        - Was ist der Unterschied zwischen [relativen und absoluten Schätzung](https://www.brainbits.net/blog/story-points-agile-punktlandung/)?
        - Weshalb macht man in [SCRUM relative Schätzungen](../Ressourcen/Scrum/Zeremonien/Refinement/relative%20vs%20absolute%20Schätzung/Readme.md) und keine absolute?
        - Timeboxed: 15 Minuten
        - Q&A im Plenum
    - Was ist [DoD (Defintion of Done)](../Ressourcen/Scrum/Definition_of_Done/Readme.md)?
        - Was ist der Unterschied von Abnahmekriterien einer Userstory und DoD?
        - Was ist der Sinn und Zweck einer DoD?
        - Timeboxed: 10 Minuten
        - Q&A im Plenum
4. Auftrag: Was passiert in den Meetings?
    - [Sprintplanning](../Ressourcen/Scrum/Zeremonien/Sprintplanning/Readme.md)

>Scrum Teams laden Lehrperson (Beobachter) zu mind. 3 Events/ Zeremonie ein. Relevant für Bewertung:
>- Daily Scrum
>- Sprintplanning
>- Refinement
>- Sprint Review
>- Sprint Retro

---
## Hands-on
**Ende Sprint 0**
- Review Sprint 0
- Retrospective Sprint 0

**Sprint 1, Woche 1**
- Start Sprint 1
  - Daily Scrum
  - Planning Sprint 1
    - Falls Schätzungen fehlen, [Refinement](../Ressourcen/Scrum/Zeremonien/Refinement/Readme.md) nachholen
  - Bearbeitung Sprint 1

---

&copy;TBZ | 28.02.2023