## Tag 05

## Programm

>Basis Check (einzeln, ecolm.com, ohne Hilfsmittel)

1. Vorträge
   - Kandidat 
   - Kandidat 
   - Kandidat 

2. Auftrag SCRUM-Teams «Was passiert in den Meetings?»
   - Sprint Review
     - [Sprint Review](../Ressourcen/Scrum/Zeremonien/Sprintreview/Readme.md)      
     - Untersuchen und im Plenum erklären
     - Timeboxed: 10 Minuten

   - Retrospective
     - [Retrospective](../Ressourcen/Scrum/Zeremonien/Retrospective/Readme.md)     
     - Untersuchen und im Plenum erklären
     - Timeboxed: 10 Minuten

3. Input: [Burn down-Chart](../Ressourcen/Scrum/Messungen/Burn%20down%20chart/Readme.md)

4. Ergänzungen
    - «Von Product Vision zu Userstories»
        - [Userstory splitten](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
    - Refinement
        - [Refinement D.E.E.P (Roman Pichler)](https://www.romanpichler.com/blog/make-the-product-backlog-deep/)
        - [Refining the Product Backlog](https://www.romanpichler.com/blog/refining-the-product-backlog/)
        - [INVEST](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/#INVEST)

---
## Hands-on

**Sprint 1, Woche 2**
- Daily Scrum
- Bearbeitung Sprint 1
- Product Backlog vorbereiten für Sprint 2
  - Priorisieren
  - [Refinement, inkl. Aufwandschätzung](../Ressourcen/Scrum/Zeremonien/Refinement/Readme.md)

---

&copy;TBZ | 28.02.2023
