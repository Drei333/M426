# Tag 03

## Programm
1. Vorträge
   - Kandidat 4
   - Kandidat 5
2. Input: [Epic/ Userstory](/Ressourcen/Scrum/epic_userstory/)
3. Input: Was passiert in den Meetings?
   - [Daily Scrum](/Ressourcen/Scrum/Zeremonien/daily_scrum.pdf)
   - [Refinement: Product Backlog verfeinern](../Ressourcen/Scrum/Zeremonien/Refinement/Readme.md)

---
## Hands-on
**Sprint 0, Woche 2**
- Reflektieren Sie nochmals den [SCRUM-Flow](https://www.youtube.com/watch?v=A26apfhIWEE)
- Versuchen Sie ihre erste Zeremonie gem. Handout: [Daily Scrum](/Ressourcen/Scrum/Zeremonien/daily_scrum.pdf) umzusetzen
  - Zusätzliche Hilfe: [Video Erklärung](https://www.youtube.com/watch?v=PbPmYbMzERg)
- Erstellen Sie ihr Product Backlog, u.a. sollen Sie Userstories entwickeln. Hilfestellungen finden Sie hier:
  - [Techniken, um Userstories zu zerlegen](https://www.dasscrumteam.com/de/user-stories)
    - [Video1 (User Stories)](https://vimeo.com/405408141)
    - [Video2 (Epics)](https://vimeo.com/405408146/562091d462)
    - [Video3 (Aspekte)](https://vimeo.com/405408155/7010173449)
    - [Poster Scrum Kompakt](/Ressourcen/Scrum/Scrum%20flow/Poster/User%20Story%20kompakt%20de.pdf)
  - [Von Product vision zu Ecpics & Userstories](https://www.mendix.com/de/blog-de/eine-praktische-anleitung-fur-den-product-canvas/)
  - [Customer journey map](https://blog.hubspot.de/marketing/customer-journey-map-erstellen)  
- Priorisieren Sie ihr Product Backlog mit einer der Methoden ([Uebersicht Methoden](https://agilesverwaltungswissen.org/wiki/Kategorie:Priorisierungsmethoden))
  - [MoSCoW-Priorisierung](https://agilesverwaltungswissen.org/wiki/MoSCoW-Priorisierung)
  - [Eisenhower Matrix](https://agilesverwaltungswissen.org/wiki/Eisenhower-Matrix)
  - [Nutzen-orientierte Priorisierung](https://agilesverwaltungswissen.org/wiki/Nutzen-orientierte_Priorisierung)
  

---

&copy;TBZ | 21.02.2023