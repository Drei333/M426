# Definition of Done

In Scrum, "Definition of Done" (DoD) ist ein zentraler Begriff, der die Kriterien oder Anforderungen beschreibt, die ein Produktinkrement oder eine Aufgabe erfüllen muss, um als abgeschlossen betrachtet zu werden. Es ist eine gemeinsame Vereinbarung innerhalb des Scrum-Teams und hilft dabei, ein gemeinsames Verständnis darüber zu schaffen, wann eine Aufgabe oder ein Sprintziel erreicht ist.

Die Definition of Done enthält typischerweise eine Liste von Anforderungen, die erfüllt sein müssen, bevor eine Aufgabe oder ein Inkrement als "fertig" angesehen wird. Diese Anforderungen können je nach Projekt und Team variieren, aber sie sollten spezifisch, messbar und erreichbar sein. Hier sind einige Beispiele für Kriterien, die in einer Definition of Done enthalten sein könnten:

1. Code-Qualität: Der Code wurde überprüft und erfüllt die festgelegten Code-Qualitätsstandards. Es wurden keine kritischen Fehler oder Sicherheitslücken gefunden.

2. Unit-Tests: Alle relevanten Unit-Tests wurden geschrieben und bestanden.

3. Integrationstests: Die Funktionen wurden erfolgreich in das Gesamtsystem integriert und getestet.

4. Dokumentation: Die notwendige Dokumentation wurde aktualisiert, einschließlich Benutzerdokumentation, API-Dokumentation und technischer Dokumentation.

5. Review: Der Code und die Änderungen wurden von anderen Teammitgliedern überprüft und genehmigt.

6. Akzeptanzkriterien: Alle in der Aufgabenbeschreibung oder im Product Backlog-Eintrag festgelegten Akzeptanzkriterien wurden erfüllt.

7. Demo: Das abgeschlossene Inkrement kann in einer Sprint-Demo vorgeführt werden.

8. Keine offenen Blocker: Es gibt keine offenen Blocker oder kritischen Hindernisse, die die Veröffentlichung oder Bereitstellung des Inkrements behindern.

Die Definition of Done sollte für jedes Team und jedes Projekt maßgeschneidert sein und regelmäßig überprüft und aktualisiert werden, um sicherzustellen, dass sie den aktuellen Anforderungen und Qualitätsstandards entspricht. Die Einhaltung der Definition of Done ist entscheidend, um sicherzustellen, dass das Team qualitativ hochwertige Arbeit leistet und die Erwartungen des Product Owners und der Stakeholder erfüllt werden.

Quellen: https://agilescrumgroup.de/definition-of-done/

![](../Definition_of_Done/Dod%20example.jpg)

![](../Definition_of_Done/Definition+of+Done+(DoD)+List+(Example).jpg)

![](../Definition_of_Done/dod%20checklist.png)