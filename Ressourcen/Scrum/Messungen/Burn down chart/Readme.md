# burn down chart

[TOC]

Eine Burn-Down-Chart ist ein visuelles Hilfsmittel, das in agilen Vorgehensmodellen wie Scrum verwendet wird, um den Fortschritt eines Projekts (Release Burn-Down-Chart) oder eines Sprints (Sprint-Burn-Down-Chart) zu verfolgen. Diese Diagramme helfen dabei, die verbleibende Arbeit über die Zeit zu visualisieren und zu veranschaulichen, ob das Team im Zeitplan liegt, um seine Ziele zu erreichen. 

Die Sprint-Burn-Down-Chart zeigt typischerweise auf der x-Achse die Zeit (normalerweise in Tagen) und auf der y-Achse die verbleibende Arbeit (normalerweise in Aufgaben, Story Points). Während des Sprints wird die verbleibende Arbeit kontinuierlich aktualisiert und aufgezeichnet.

Hier sind einige wichtige Aspekte einer Burn-Down-Chart:

1. **Ist-Linie:** Diese Linie zeigt den tatsächlichen Verlauf der verbleibenden Arbeit über die Zeit. Sie beginnt am Anfang des Sprints bei einem bestimmten Wert (der die geplante Arbeit darstellt) und fällt idealerweise linear auf null, wenn der Sprint abgeschlossen ist.

2. **Ideallinie:** Diese Linie repräsentiert die ideale Arbeitsoptik, die notwendig wäre, um den Sprint pünktlich abzuschließen. Sie ist normalerweise eine gerade Linie vom Startpunkt bis zum Endpunkt und zeigt, wie die Arbeit idealerweise abnehmen sollte.

3. **Verbleibende Arbeit:** Die tatsächlichen Punkte oder Werte, die den verbleibenden Arbeitsaufwand zu verschiedenen Zeitpunkten im Sprint darstellen. Diese Punkte werden anhand der tatsächlichen Fortschritte und der erledigten Arbeit aktualisiert.

![](burndownchart.jpg)

Die Burn-Down-Chart bietet eine schnelle und leicht verständliche Möglichkeit für das Team, den Fortschritt zu überwachen und zu erkennen, ob es auf Kurs ist, um seine Ziele zu erreichen. Wenn die Ist-Linie unterhalb der Ideallinie liegt, zeigt dies an, dass das Team im Zeitplan liegt. Wenn die Ist-Linie über der Ideallinie liegt, deutet dies darauf hin, dass das Team hinter dem Zeitplan zurückliegt.

Die Burn-Down-Chart ist ein effektives Werkzeug für die Kommunikation innerhalb des Teams und mit den Stakeholdern, da sie auf einen Blick zeigt, wie das Projekt voranschreitet. Es ermöglicht auch die Identifizierung von Problemen oder Verzögerungen frühzeitig, damit das Team entsprechend reagieren kann.

## Quellen
- [Burndownchart.pdf](burn%20down%20chart.pdf)