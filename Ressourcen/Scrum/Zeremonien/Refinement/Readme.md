# Refinement

[TOC]

Im Scrum-Framework ist das Refinement (auch bekannt als "Backlog-Refinement" oder "Product Backlog Refinement") eine regelmäßige Aktivität, die dazu dient, den Product Backlog zu pflegen und sicherzustellen, dass die darin enthaltenen Elemente klar, priorisiert und für die künftige Umsetzung bereit sind. 

**Das Refinement ist keine offizielle Scrum-Veranstaltung, sondern eine kontinuierliche Aktivität, die während der Sprint-Planung und in der gesamten Sprint-Dauer stattfindet.**

Während des Refinements führt das Scrum-Team in der Regel folgende Aktivitäten durch:

1. **Product Owner verfeinert die Elemente:** Der Product Owner ist dafür verantwortlich, die Elemente im Product Backlog regelmäßig zu überprüfen und zu verfeinern. Dies kann das Hinzufügen neuer Elemente, das Entfernen von nicht mehr relevanten Elementen, das Aktualisieren von Beschreibungen oder Akzeptanzkriterien sowie das Anpassen von Prioritäten und Schätzungen umfassen.

2. **Verständnis und Klarheit schaffen:** Das Team arbeitet mit dem Product Owner zusammen, um sicherzustellen, dass die Backlog-Elemente ausreichend klar und verständlich sind. Dies kann das Klären von Unklarheiten in den Anforderungen oder das Hinzufügen weiterer Details und Beispiele einschließen.

3. **Schätzungen durchführen bzw. Schätzungen aktualisieren:** Wenn nötig, aktualisiert das Entwicklungsteam die [Schätzungen](./Planning%20Poker/Readme.md) für die Elemente im Product Backlog. Dies kann aufgrund neuer Informationen oder eines besseren Verständnisses der Anforderungen erfolgen.

4. **Priorisierung:** Das Scrum-Team kann die Prioritäten der Elemente überprüfen und sicherstellen, dass die wichtigsten Elemente ganz oben auf der Prioritätenliste stehen.

5. **Zielsetzung:** Das Refinement ermöglicht es dem Team und dem Product Owner, die Ziele für zukünftige Sprints zu klären und zu definieren, welche Elemente in den nächsten Sprints angegangen werden sollen.

6. **Vorbereitung für die Sprint-Planung:** Während des Refinement werden die Backlog-Elemente in einen Zustand versetzt, in dem sie für die Auswahl und Planung in einem kommenden Sprint bereit sind. Dies erleichtert die Sprint-Planung und stellt sicher, dass das Team genügend Details hat, um die Arbeit abzuschätzen und zu verpflichten.

Das Refinement ist wichtig, um sicherzustellen, dass der Product Backlog aktuell und gut vorbereitet ist, damit das Team bei der Sprint-Planung effizient arbeiten kann. Es trägt auch dazu bei, die Transparenz und die Zusammenarbeit zwischen dem Product Owner und dem Entwicklungsteam zu verbessern, da sie gemeinsam am Verständnis der Anforderungen und der Ausrichtung auf die Produktziele arbeiten. Es ist eine kontinuierliche Aktivität und kann in verschiedenen Formen und Häufigkeiten je nach den Bedürfnissen des Teams durchgeführt werden.

## Schätzung mit Planning Poker
Lesen Sie [hier alles über Planning Poker](./Planning%20Poker/).

## relative vs. absolute Schätzung
Es gibt sehr viele Diskussionen bezüglich der Schätzung in SCRUM. Lesen Sie [hier die Argumente](./relative%20vs%20absolute%20Schätzung/Readme.md), weshalb sich die relative Schätzung besser eignet.

## Quellen
- Handout: [Refinement.pdf](Refinement.pdf)
- Video: https://www.youtube.com/watch?v=qzTAOlWKZuw&t=216s
- [Userstory splitten](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/)
- [Refinement D.E.E.P (Roman Pichler)](https://www.romanpichler.com/blog/make-the-product-backlog-deep/)
- [Refining the Product Backlog](https://www.romanpichler.com/blog/refining-the-product-backlog/)
- [INVEST](https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/#INVEST)

---