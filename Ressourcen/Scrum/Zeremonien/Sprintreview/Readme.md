# Sprintreview

Das Sprint-Review ist ein wichtiges Ereignis im Scrum-Framework und findet am Ende eines jeden Sprints statt. Während des Sprint-Reviews präsentiert das Entwicklungsteam die im abgeschlossenen Sprint erstellten Inkremente dem Product Owner, dem Scrum-Master, den Stakeholdern und anderen interessierten Parteien. Das Hauptziel des Sprint-Reviews ist es, Transparenz über die abgeschlossene Arbeit zu schaffen, Feedback zu sammeln und den Fortschritt des Projekts zu überprüfen.

Hier eine kurze Abfolge, was in einem Sprint-Review passiert:

1. **Sprint-Review-Meeting:** Das Sprint-Review ist ein formelles Meeting, das üblicherweise eine begrenzte Zeit dauert, abhängig von der Länge des Sprints (normalerweise ein paar Stunden). Alle Mitglieder des Scrum-Teams, einschließlich des Product Owners, des Scrum-Masters und des Entwicklungsteams, nehmen daran teil. Außerdem sind Stakeholder wie Kunden, Benutzer, Management und andere interessierte Parteien eingeladen.

2. **Präsentation des abgeschlossenen Inkrements:** Das Entwicklungsteam präsentiert das potenziell auslieferbare Produktinkrement, das während des Sprints erstellt wurde. Dies umfasst normalerweise eine Demonstration der neuen Funktionen oder Änderungen, die während des Sprints umgesetzt wurden. Das Ziel ist es, zu zeigen, was erreicht wurde und wie es mit dem Sprint-Ziel und den Product-Backlog-Elementen in Einklang steht.

3. **Feedback und Diskussion:** Die Stakeholder, einschließlich des Product Owners und anderer Teilnehmer, geben Feedback zu der vorgestellten Arbeit. Sie können Fragen stellen, Klarstellungen verlangen und Einblicke oder Vorschläge zur Verbesserung geben. Dies ist eine kollaborative Diskussion, die dazu dient, das Verständnis zu vertiefen und mögliche Anpassungen oder Änderungen zu diskutieren.

4. **Überprüfung des Sprint-Ziels:** Das Team überprüft, ob das Sprint-Ziel erreicht wurde, und diskutiert, ob es möglicherweise Anpassungen für zukünftige Sprints geben sollte.

5. **Priorisierung des Product Backlog:** Basierend auf dem Feedback und den Erkenntnissen aus dem Sprint-Review kann der Product Owner den Product Backlog überprüfen und die Prioritäten für zukünftige Arbeiten aktualisieren.

6. **Abschluss des Sprint:** Das Sprint-Review markiert den Abschluss des aktuellen Sprints. Eventuell unvollständige Arbeiten oder offene Fragen können in den nächsten Sprint übernommen werden.

**Das Sprint-Review fördert die Zusammenarbeit und die offene Kommunikation zwischen dem Scrum-Team und den Stakeholdern.** Es trägt dazu bei, sicherzustellen, dass das Produkt in die gewünschte Richtung entwickelt wird und dass das Team ständig auf das Feedback und die Anforderungen der Stakeholder reagieren kann. Das Sprint-Review ist ein entscheidender Schritt im Scrum-Entwicklungsprozess, um sicherzustellen, dass das Produkt den Erwartungen und Bedürfnissen der Nutzer entspricht.

---
Handout: [Sprintreview.pdf](Sprint%20Review.pdf)

Video: https://www.youtube.com/watch?v=xXs9ooerCMk