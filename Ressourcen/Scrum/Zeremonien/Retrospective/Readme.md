# Retrospective

[TOC]

In der Sprint-Retrospektive in Scrum handelt es sich um eine spezielle Besprechung am Ende eines jeden Sprints, die dazu dient, den vergangenen Sprint zu reflektieren und Verbesserungen zu identifizieren. Die Retrospektive ermöglicht es dem Scrum-Team, sich auf das zu konzentrieren, was gut gelaufen ist und was verbessert werden kann, um die Arbeitsweise und die Effizienz im nächsten Sprint zu steigern. Hier ist, was in einer typischen Sprint-Retrospektive in Scrum passiert:

1. **Sammeln des Scrum-Teams:** Alle Mitglieder des Scrum-Teams, einschließlich des Product Owners, des Scrum Masters und des Entwicklungsteams, nehmen an der Retrospektive teil. Dies ist eine Gelegenheit für offene und ehrliche Kommunikation.

2. **Rückblick auf den Sprint:** Das Team beginnt damit, die Ereignisse, Aktivitäten und Ergebnisse des gerade abgeschlossenen Sprints zu überprüfen. Dies umfasst eine Diskussion darüber, was gut gelaufen ist und was nicht wie geplant verlief.

3. **Identifikation von Stärken:** Das Team identifiziert die positiven Aspekte und Erfolge des Sprints. Dies können beispielsweise effektive Zusammenarbeit, pünktliche Lieferungen oder Qualitätsgewinne sein.

4. **Identifikation von Verbesserungsbereichen:** Das Team bespricht auch die Herausforderungen, Probleme oder Unzulänglichkeiten, die während des Sprints aufgetreten sind. Dies kann Bereiche betreffen, in denen die Effizienz gesteigert, die Qualität verbessert oder Hindernisse beseitigt werden müssen.

5. **Erarbeiten von Maßnahmen:** Das Team entwickelt konkrete Maßnahmen oder Aktionspunkte, um die identifizierten Verbesserungsbereiche anzugehen. Diese Maßnahmen sollten spezifisch, messbar und umsetzbar sein.

6. **Priorisierung:** Wenn mehrere Verbesserungsbereiche identifiziert wurden, kann das Team die Prioritäten setzen und entscheiden, welche Maßnahmen zuerst umgesetzt werden sollen.

7. **Verpflichtung zur Verbesserung:** Das Scrum-Team verpflichtet sich, die vereinbarten Maßnahmen im nächsten Sprint umzusetzen und die Fortschritte während der nächsten Retrospektive zu überprüfen.

8. **Dokumentation:** Die Ergebnisse der Retrospektive, einschließlich der identifizierten Maßnahmen, werden dokumentiert, um die Fortschritte im Laufe der Zeit nachvollziehbar zu machen.

Die Sprint-Retrospektive ist ein Schlüsselaspekt von Scrum, der die kontinuierliche Verbesserung fördert und sicherstellt, dass das Team in der Lage ist, sich anzupassen und effizienter zu arbeiten. Es ist ein wichtiger Schritt, um die Arbeitsweise des Teams und die Produktqualität kontinuierlich zu verbessern.

## Quellen
- Handout: [Retrospective.pdf](Sprint%20Retrospective.pdf)
- Video: https://www.youtube.com/watch?v=xXs9ooerCMk
- Starfish Anleitung: https://echometerapp.com/de/seestern-retrospektive/
- Verschiedene Retro Techniken: https://retromat.org/
- [Starfish Retro Drehbuch](Retro%20Starfish%20Drehbuch.pdf)
- [Präsentation Retrospective](Präsentation%20Retrospective.pdf)

---