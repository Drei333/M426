# Vortraege

[TOC]

## Vortragsthemen
- [Scummaster und PO](./Vortragsthemen/Scrummaster%20PO.md)
- [Stakeholder](./Vortragsthemen/Stakeholder.md)
- [Scrum tools](./Vortragsthemen/scrumtools.md)
- [Storypoints](./Vortragsthemen/Storypoints.md)
- [Designpatterns](./Vortragsthemen/Designpatterns.md)
- [Cleancode/ Refactoring](./Vortragsthemen/Cleandcode%20Refactoring.md)
- [Wiederverwendbarkeit von Code](./Vortragsthemen/Wiederverwendbarkeit%20von%20Code%20-%20Copy.md)
- [CI/CD](./Vortragsthemen/CICD.md)
- [Automatisches Testen](./Vortragsthemen/Automatische%20Tests.md)
- [VCS](./Vortragsthemen/VCS.md)
- [git vs subversion](./Vortragsthemen/git%20vs%20subversion.md)
- [VCS tools](./Vortragsthemen/VCS%20Tools.md)
- [XP Extreme Programming](./Vortragsthemen/XP.md)
- [SOLID](./Vortragsthemen/SOLID.md)
- [Code Quality Tools](./Vortragsthemen/code%20quality%20tools.md)

## Vortragsbewertung
Die Bewertung findet mit folgendem [Vortragsbewertungraster](Vortragsbewertungraster.pdf) am gleichen Tag statt. Die Lehrperson führt ein persönliches Feedbackgespräch durch.

## Tipps
- [10 Dinge die Sie bei einem Vortrag unbedingt beachten sollten](https://wb-web.de/material/medien/10-dinge-die-sie-bei-prasentationen-dringend-beachten-sollten.html)
- [Wie halte ich einen Vortrag](./Tipps/wie%20halte%20ich%20einen%20vortrag.pdf)
- [Wie halte ich einen online Vortrag](https://www.meg-denkwelt.at/blog/tipps-f%C3%BCr-einen-online-vortrag-f%C3%BCr-vortragende)


---
