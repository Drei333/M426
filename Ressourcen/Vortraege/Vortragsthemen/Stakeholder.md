# Stakeholder

[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Was versteht man unter dem Begriff "Stakeholder" im Kontext von Unternehmen oder agilen Vorhaben?

Recherchieren Sie und versuchen Sie folgende Fragen zu beantworten:
- Welche Arten von Stakeholdern unterscheiden sich?
- Zählen Sie einige Eigenschaften eines Stakeholders auf
- Warum ist die Kommunikation mit einem Stakeholder so wichtig?
- Welchen Einfluss hat ein Stakeholder auf den Erfolg eines agilen Vorhabens?
- Könnten Sie sich vorstellen, in der Rolle als Stakeholder zu arbeiten?

Als Einstieg hilft Ihnen vielleicht diese Quelle:</br>
https://www.agile-heroes.com/de/magazine/scrum-stakeholder/

---
