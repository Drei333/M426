# SOLID


[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

SOLID ist eine Abkürzung für fünf grundlegende Designprinzipien in der objektorientierten Programmierung und Softwareentwicklung. 
- Von wem wurde dieser SOLID-Prinzip entwickelt und mit welcher Absicht?
- Erklären Sie anhand eines einfachen Beispiels, wie jedes Designprinzip zu verstehen ist.
- Welche Erfahrungen haben Sie gemacht? Haben Sie vielleicht bewusst/unbewusst solche Prinzipien auch eingesetzt?


---
