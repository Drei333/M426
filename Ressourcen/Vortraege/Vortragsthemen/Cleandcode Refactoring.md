# Cleancode und Refactoring

[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Gewünscht wird eine Einleitung, eine allgemeine Erklärung 
und Live-Demos, wenn möglich aus Ihrem Praxis-Alltag.

Aufzeigen, was Clean-Code ist und warum. 
Suchen Sie im Internet nach allgemein gültigen 
Richtlinien für "guten" Code.
Zeigen Sie anhand einer Programmiersprache beispielhafte 
"gute" Code-Beispiele und "gute" Muster auf. 
Zeigen Sie auch "schlechte" Beispiele auf und erklären Sie warum.



---
