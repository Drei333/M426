# VCS (Version Control System)


[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Zeigen Sie im Detail, wie Git funktioniert
Wie die Repository-Struktur aussieht und wo welches
Repository liegt. Zeigen Sie den Unterschied und die 
Arbeitsweise von "master" und "origin".

Zeigen Sie neben den "alltäglichen" Befehlen wie
"git pull" und "git push" (was alle kennen) auch 
andere nützliche Befehle und Funktionen wie z.B. 
Konfliktlösungen, Spezial- oder Release-Branches usw.

Eine Live-Demo wäre toll (achten Sie darauf, dass der 
Beamer Mühe hat mit schwarzem Hintergrund und kleiner 
Schrift! Also für die Demo ein helles Grau einstellen). 
Vielleicht geben Sie der Klasse auch ein Handout 
mit dem Wichtigsten ab. Entscheiden Sie selbst.



---
