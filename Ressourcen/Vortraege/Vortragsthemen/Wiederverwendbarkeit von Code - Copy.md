# Wiederverwendbarkeit von Code

[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Zeigen Sie den Zuhörern auf, wie in einem guten Code die
Wiederverwendbarkeit zu machen ist mit guten und schlechten
Beispielen.

Auf wie viel oder unterschiedliche Arten ist dies zu machen?
(Funktionen, Methoden, Vererbung, Interfaces, Module, Library,
API, Fernzugriffe wie RPC [remote procedure call] oder REST)
und anderes

Live-Demo's sind empfehlenswert, aber achten Sie auf die Zeit!


---
