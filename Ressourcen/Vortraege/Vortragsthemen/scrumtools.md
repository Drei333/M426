# Scrumtools


[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Für diesen Vortrag wollen wir gerne sehen, 
- wie die verschiedenen SCRUM-Tools funktionieren
- was sie können, was "cool" ist, wie sie sich anfühlen und vielleicht, was sie nicht können
- was sie kosten (Gegenüberstellung)
- ob es Verbindungen zu Versionsverwaltung oder Terminplaner gibt

Es sollen mindestens 2, besser 3 SCRUM-Tools miteinander / gegeneinander verglichen werden.
Versuchen Sie auch einen kleinen Marktüberblick zu geben, was aktuell verfügbar ist.

Wenn Sie selber ein SCRUM-Tool im Einsatz haben in Ihrem Betrieb, wäre es schön, wenn Sie ein paar ScreenShots daraus zeigen könnten. 
(Achtung: Decken Sie Projektname und vertrauliche Dinge ab)


---
