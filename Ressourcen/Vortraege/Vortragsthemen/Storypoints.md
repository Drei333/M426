# Storypoints

[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

- Die Einführung in das SCRUM Vorgehensmodell beinhaltet das Konzept der "Story Points". 
- Aber was sind Story Points und warum sind sie wichtig? Wie unterscheiden sich von herkömmlichen Zeitschätzungen?
- Wie genau tragen sie zur Arbeitsaufwandsplanung bei? Die Skala der Story Points, basiert auf eine Zahlenfolge, welche?
- Welche Faktoren beeinflussen die Vergabe von Story Points? 
- Welche Techniken erleichtern die Schätzung mit Storypoints. 
- Welche Vorteile bringen die Aufwandschätzungen mit Storypoints in einer agilen Umgebung mit sich?
- Haben Sie bereits mit Storypoints gearbeitet? Wenn ja, welche Erfahrungen haben Sie damit gemacht? Wenn nein, welche Herausforderungen sehen Sie hier?

---
