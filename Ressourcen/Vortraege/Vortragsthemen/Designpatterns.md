# Designpatterns

[TOC]

## Inhalt

Zeitvorgabe: 7 Min.

Zeigen Sie den Zuhörern auf, welche Design-Patterns / Entwurfsmuster es gibt (Gruppen) und zeigen Sie anschliessend ein oder zwei Design-Pattern im Detail. Aufbau und Einsatz.

Machen Sie auch eine Einleitung wer dies "erfunden" hat.

https://de.wikipedia.org/wiki/Entwurfsmuster


---
