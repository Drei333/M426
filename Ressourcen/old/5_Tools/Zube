<!doctype html>
<html class="no-js">
    <head itemscope>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Zube | Agile project management with a seamless GitHub integration</title>
        <meta name="description" content="Zube lets the product team work alongside the developers. The GitHub integration always keeps developer issues up to date.">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory. -->
        <link rel="icon" type="image/ico" href="/favicons/76806d705602d86fde0ff7533a567bcd.favicon.ico">
        <link rel="apple-touch-icon" sizes="57x57" href="/favicons/4ee898306d181155970a3ec379fea6b5.apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicons/a7cbf04e1e51ceffd5b8b4715caa9bec.apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicons/45a0045b5828a0c8719175df9cff4d73.apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicons/99bd244d78e20f6a8b6384ac6b450c95.apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicons/0df6ea3629cd40e9b11bd3b0c793a998.apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicons/21a05b3a4a7db786a72082f5aa74f9b8.apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicons/be4b6500f270210c3437facd3322c28b.apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicons/4152852cd3be1207fb77de56a272a6cc.apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/083e6470c41a37c8dbb5b5d4206078b5.apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="/favicons/a82779e90d4d46deb2dbc68e0971352b.android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/20612186ed82559c9ddfff6f843ef1f4.favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicons/9437c2b344073f0867b0f3f15179dd35.favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/2bdcc1c418cd50d7b58214b7ccf083a8.favicon-16x16.png">
        <link rel="manifest" href="/favicons/b58fcfa7628c9205cb11a1b2c3e8f99a.manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicons/be4b6500f270210c3437facd3322c28b.ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link rel="canonical" href="https://zube.io">
        <!-- open graph tags -->
        <meta property="og:title" content="Zube | Agile project management with a seamless GitHub integration">
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://zube.io">
        <meta property="og:image" content="https://zube.io/blog/images/assets/fbog_logo_with_computer.png">
        <meta property="og:site_name" content="Zube">
        <meta property="og:description" content="Zube lets the product team work alongside the developers. The GitHub integration always keeps developer issues up to date.">
        <meta property="fb:app_id" content="332989173491459">
        <!-- twitter cards -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@zubeio">
        <meta name="twitter:description" content="Zube lets the product team work alongside the developers. The GitHub integration always keeps developer issues up to date.">
        <meta name="twitter:image" content="https://zube.io/blog/images/assets/fbog_logo_with_computer.png">

        <link rel="stylesheet" href="/styles/84ef85767c665d985cfcfb58df23d13a.main.css">
        <script src="/scripts/vendor/6bc355963213c95dbb985716ae29a070.modernizr.js"></script>

        <!-- Google Webfonts for Bootstrap Landing -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
        <link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    </head>
    <body id="home" class="static-page">
        <!--[if lt IE 10]>
            <p class='browsehappy'>You are using an <strong>outdated</strong> browser. Please <a href='http://browsehappy.com/'>upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Header -->
        <nav class="navbar navbar-default navbar-fixed-top header" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand logo"><i class="zubeicon-zube-logo"></i></a>
                </div>


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/pricing">Pricing</a></li>
                        <li><a href="/docs">Docs</a></li>
                        <li><a href="/docs/api">API</a></li>
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/contact">Contact</a></li>
                        <li class="dropdown">
                            <a class="btn-login dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Log In
                            </a>
                            <div class="dropdown-menu">
                                <div>
                                    <a class="btn provider-btn google-btn btn-login" href="/auth/google">
                                        <img class="vendor-icon" src="/images/static/vendor/6c296e3f8c447510c172f9ceacce6e1a.google_login_btn.svg">
                                        <span>Log in with Google</span>
                                    </a>
                                </div>
                                <div>
                                    <a class="btn provider-btn github-btn btn-login" href="/auth/github">
                                        <img class="vendor-icon" src="/images/static/vendor/d227c5b5effd0800f782b5060eb99a7e.github_login_btn.svg">
                                        <span>Log in with GitHub</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
        <!-- Header -->

        <!-- top Content -->
        <div class="top-header">
            <div class="gradient-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="headline">
                            <h1>Developer collaboration, solved.</h1>
                            <h2>
                              Zube's powerful integration with GitHub Issues makes it easy for the entire team to stay on the same page.
                            </h2>
                            <div class="cta">
                                <p>Get started for free</p>
                                <a class="btn provider-btn google-btn btn btn-signup word-wrap btn-lg" href="/auth/google">
                                    <img class="vendor-icon" src="/images/static/vendor/6c296e3f8c447510c172f9ceacce6e1a.google_login_btn.svg">
                                    <span>Sign up with Google</span>
                                </a>
                                <a class="btn provider-btn github-btn btn btn-signup word-wrap btn-lg" href="/auth/github">
                                    <img class="vendor-icon" src="/images/static/vendor/d227c5b5effd0800f782b5060eb99a7e.github_login_btn.svg">
                                    <span>Sign up with GitHub</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /.container -->
            </div>
            <div>
                <i class="zubeicon-epic" style="top: 20px;right: -28px;font-size: 80px;"></i>
                <i class="zubeicon-epic" style="top: 258px;right: 13px;font-size: 65px;"></i>
                <i class="zubeicon-epic" style="top: 100px;right: 215px;font-size: 45px;"></i>
                <i class="zubeicon-epic" style="top: 352px;right: -54px;font-size: 100px;"></i>
                <i class="zubeicon-epic" style="top: 500px;right: 6px;font-size: 70px;"></i>
                <i class="zubeicon-epic" style="top: 162px;right: 16px;font-size: 40px;"></i>
                <i class="zubeicon-epic" style="top: 177px;right: 183px;font-size: 89px;"></i>
                <i class="zubeicon-epic" style="top: 393px;right: 107px;font-size: 78px;"></i>
                <i class="zubeicon-epic" style="top: 45px;right: 131px;font-size: 60px;"></i>
                <i class="zubeicon-epic" style="top: 319px;right: 251px;font-size: 49px;"></i>
                <i class="zubeicon-epic" style="top: 449px;right: 227px;font-size: 40px;"></i>
                <i class="zubeicon-epic" style="top: 500px;right: 300px;font-size: 95px;"></i>
                <i class="zubeicon-epic" style="top: 385px;right: 286px;font-size: 69px;"></i>
                <i class="zubeicon-epic" style="top: 162px;right: 100px;font-size: 54px;"></i>
                <i class="zubeicon-epic" style="top: 296px;right: 121px;font-size: 55px;"></i>
                <i class="zubeicon-epic" style="top: 511px;right: 156px;font-size: 57px;"></i>
                <i class="zubeicon-epic" style="top: 100px;right: 63px;font-size: 40px;"></i>
                <i class="zubeicon-epic" style="top: 607px;right: -10px;font-size: 40px;"></i>
                <i class="zubeicon-epic" style="top: 658px;right: 69px;font-size: 76px;"></i>
                <i class="zubeicon-epic" style="top: 700px;right: -8px;font-size: 48px;"></i>
                <i class="zubeicon-epic" style="top: 596px;right: 106px;font-size: 40px;"></i>
                <i class="zubeicon-epic" style="top: 635px;right: 200px;font-size: 88px;"></i>
            </div>
        </div>
        <!-- top Content -->

        <!-- Logos -->
        <div id="customer-logos" class="wrap-content wrap-content-white">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Used by the world's best teams</h2>
                    </div>
                    <div class="col-xs-12">
                        <img src="images/static/landing/logos/e427e8757a989376e3877d50f9af4585.autodesk.svg" style="height: 30px;">
                        <img src="images/static/landing/logos/aaf981a5f774139bb2e56dc05a071b48.ipsy.svg" style="height: 30px; margin-top:20px;">
                        <img src="images/static/landing/logos/a054203a8668173e33796773ffa3c9a4.universal.svg" style="height: 50px;">
                        <img src="images/static/landing/logos/f524cd16746759f27f2a24c0b6c8e7f8.meltwater.svg" style="height: 35px;">
                        <img src="images/static/landing/logos/85c4a49ee13d32161400fb9bd94e9e54.bridg.svg" style="height: 30px;">
                    </div>
                </div>
            </div>
        </div>
        <!-- Logos -->

        <div class="divider"></div>

        <!-- Info Content -->
        <div class="wrap-content wrap-content-blue">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-container">
                        <hr class="heading-spacer">
                        <div class="clearfix"></div>
                        <h2>Agile project management with a <br><span class="highlight">beautiful</span> and <span class="highlight">blazing fast</span> interface.</h2>
                        <p class="lead">
                            The kanban board provides an Agile workflow out of the box. Get a clear overview of your project, or take a closer look with advanced filtering.
                        </p>
                    </div>
                    <div class="col-lg-6 img-container">
                        <img class="img-responsive" src="images/static/landing/4b9a3a5655ccffe2c47ca5519ae1f1cd.zube-kanban-board.png" alt="Zube project management kanban board">
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div>
        <!-- Info Content -->

        <div class="divider"></div>

        <!-- Info Content -->
        <div class="wrap-content wrap-content-teal">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-push-5 text-container">
                        <hr class="heading-spacer">
                        <div class="clearfix"></div>
                        <h2>The <span class="highlight">source of truth</span>. Never copy and paste another <span class="highlight">developer task</span>.</h2>
                        <p class="lead">
                            Seamlessly work between Zube and GitHub Issues. Attach one or multiple GitHub repositories to your Zube project and your data will always stay up to date in both platforms. On Zube, everyone can collaborate on developer tasks, even if they are not developers.
                        </p>
                    </div>
                    <div class="col-lg-4 col-lg-pull-5 img-container">
                        <img id="connected-diagram" class="img-responsive" src="images/static/landing/712bd80750d883f1ffb888598d97dba0.connected_diagram.svg" alt="Zube project management tool connects everyone on your team">
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div>
        <!-- Info Content -->

        <div class="divider"></div>

        <!-- Info Content -->
        <div class="wrap-content wrap-content-purple">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 col-lg-push-1 text-container">
                <hr class="heading-spacer">
                <div class="clearfix"></div>
                <h2>Everything you need to <span class="highlight">plan</span> and <span class="highlight">manage</span> your product.</h2>
                <p class="lead">
                  Agile Epics help you to accomplish your larger goals. Scrum style Sprints keep your team focused and accountable. Customer support tickets make sure critical issues are resolved and communicated effectively.
                </p>
              </div>
              <div class="col-lg-6 components-container">
                <div class="icon-item sprints">
                  <i class="zubeicon-sprintboard"></i>
                  <strong>Sprints</strong>
                </div>
                <div class="icon-item epics">
                  <i class="zubeicon-epic"></i>
                  <strong>Epics</strong>
                </div>
                <div class="icon-item tickets">
                  <i class="zubeicon-tickets"></i>
                  <strong>Customer Support <br>Tickets</strong>
                </div>
              </div>
            </div>
          </div>
          <!-- /.container -->
        </div>
        <!-- Info Content -->

        <div class="divider"></div>

        <!-- Info Content -->
        <div class="wrap-content wrap-content-blue analytics">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 text-container">
                        <hr class="heading-spacer">
                        <div class="clearfix"></div>
                        <h2>Powerful <span class="highlight">analytics</span> so you’ll know exactly when your product will <span class="highlight">ship</span>.</h2>
                        <p class="lead">
                            Analyze all of your work in one place, even your GitHub data. Burndown, Burnup, and Velocity charts show your progress. Throughput charts give insight into what's getting done.
                        </p>
                    </div>
                    <div id="chart-container" class="img-container">
                        <img class="img-responsive" src="images/static/landing/2435e63458dc93a944f31292a6131d39.chart.svg" alt="Zube project management data and analytics">
                    </div>
                </div>
            </div>
            <!-- /.container -->
        </div>
        <!-- Info Content -->

        <!-- Testimonials -->
        <div id="testimonials" class="testimonials">
            <div class="container">
                <div class="row">

                    <!-- Testimonial - 1 -->
                    <div class="col-md-4 text-center">
                        <div class="testimonial-item">
                            <img src="images/static/customers/312d99fc827fc9e62a166f5113783a1e.liam_horne.png" alt="Liam Horne, Piinpoint">
                            <h4>Liam Horne</h4>
                            <p class="job-title">CTO &sdot; PiinPoint</p>
                            <p class="testimonial">Zube lets us rapidly prioritize our backlog and manage engineering milestones so we can focus on delivering value to our customers.</p>
                        </div>
                    </div>
                    <!-- Testimonial - 2 -->
                    <div class="col-md-4 text-center">
                        <div class="testimonial-item">
                            <img src="images/static/customers/6606fabff6e1a744f470c33db679dc03.seth_pollack.png" alt="Seth Pollack, Rival IQ">
                            <h4>Seth Pollack</h4>
                            <p class="job-title">Co-Founder &sdot; Rival IQ</p>
                            <p class="testimonial">Zube makes it dead simple to see ownership of and progress against GitHub issues. Zube improves our effectiveness. Recommended!</p>
                        </div>
                    </div>
                    <!-- Testimonial - 3 -->
                    <div class="col-md-4 text-center">
                        <div class="testimonial-item">
                            <img src="images/static/customers/d793cd3bf27d57bc922ead52c2d4aa84.assaf_arkin.png" alt="Assaf Arkin, Broadly">
                            <h4>Assaf Arkin</h4>
                            <p class="job-title">CTO &sdot; Broadly</p>
                            <p class="testimonial">Zube helps us plan our work week, track what everyone is working on, and ship features on time.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials -->

        <!-- Final CTA -->
        <div id="final-cta-container" class="cta-container cta">
            <div class="container">
                <div class="row">
                    <h2>Start your 30 day free trial now.</h2>
                    <h3>Getting started is fast, easy and free.</h3>
                    <h3>No credit card required.</h3>
                    <a class="btn provider-btn google-btn btn-lg btn-signup word-wrap" href="/auth/google">
                        <img class="vendor-icon" src="/images/static/vendor/6c296e3f8c447510c172f9ceacce6e1a.google_login_btn.svg">
                        <span>Sign up with Google</span>
                    </a>
                    <a class="btn provider-btn github-btn btn-lg btn-signup word-wrap" href="/auth/github">
                        <img class="vendor-icon" src="/images/static/vendor/d227c5b5effd0800f782b5060eb99a7e.github_login_btn.svg">
                        <span>Sign up with GitHub</span>
                    </a>
                    <div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Contact info -->
        <div class="contact-info">
            <div class="container">
                <div class="row">
                    <div>
                        <h2>Questions? <span class="break-hack"></span> Contact us!</h2>
                    </div>
                </div>

                <div class="row info">
                    <div>
                        <p>415.944.9823 <span class="break-hack"></span> <a href="mailto:team@zube.io" target="_top">team@zube.io</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact info -->

        <!-- Footer Content -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-inline">
                            <li><a href="/pricing">Pricing</a></li>
                            <li>&sdot;</li>
                            <li><a href="/docs">Docs</a></li>
                            <li>&sdot;</li>
                            <li><a href="/docs/api">API</a></li>
                            <li>&sdot;</li>
                            <li><a href="/contact">Contact</a></li>
                            <li>&sdot;</li>
                            <li><a href="/blog">Blog</a></li>
                            <li>&sdot;</li>
                            <li><a href="/tos">Terms and Privacy</a></li>
                        </ul>
                        <p class="copyright text-center medium">Copyright &copy; Pivit Inc. <span id="copyright-date"></span>. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Content -->

        <!-- javascript files for ALL static pages here -->
        <script src="/scripts/350f3eeff861ee7536c269adbae657f3.vendor_static.js"></script>

        <script src="/scripts/9cc5e82050f3760ad828b47f8c28b746.plugins_static.js"></script>





        <!-- Tracking -->
        <script>
            if (location.hostname === 'zube.io' && !(document.referrer && (document.referrer.match('jenniferdewalt.com') || document.referrer.match('yumhacker.com')))) {
                // google
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-59920113-2', 'auto');
                ga('send', 'pageview');

                !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '1667832963496342');
                fbq('track', 'PageView');
            } else {
                window.ga = function () {}; // noop
                window.fbq = function () {}; // noop
            }
        </script>

        <!-- conversion event, signup -->
        <script>
            $('.btn-signup').on('click', function (e) {
                e.preventDefault();
                window.ga('send', {
                    'hitType': 'event',
                    'eventCategory': 'button',
                    'eventAction': 'click',
                    'eventLabel': 'signup'
                });
                fbq('track', 'CompleteRegistration', { value: 10.00, currency: 'USD' });
                $('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1918340&conversionId=2033380&fmt=gif" />').appendTo('body');
                window.ga('send', {
                    'hitType': 'event',
                    'eventCategory': 'button',
                    'eventAction': 'click',
                    'eventLabel': 'login'
                });
                fbq('track', 'LogIn'); // exclusion event for people who login with the signup button
                window.location = $(this).attr('href');
            });
        </script>

        <!-- tracking event, LogIn -->
        <script>
            $('.provider-btn.btn-login').on('click', function (e) {
                e.preventDefault();
                window.ga('send', {
                    'hitType': 'event',
                    'eventCategory': 'button',
                    'eventAction': 'click',
                    'eventLabel': 'login'
                });
                fbq('track', 'LogIn');
                window.location = $(this).attr('href');
            });
        </script>

        <!-- conversion event, read page -->
        <script>
            var $el = $('#testimonials');
            var $window = $(window);
            var triggerWhenOnPage = function () {
                if ($el.offset().top <= $window.scrollTop() + $window.height()) {
                    window.ga('send', {
                        'hitType': 'event',
                        'eventCategory': 'content',
                        'eventAction': 'view',
                        'eventLabel': 'testimonials'
                    });
                    fbq('track', 'ViewContent', { value: 2.00, currency: 'USD' });
                    $('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1918340&conversionId=2033388&fmt=gif" />').appendTo('body');
                    $('body').off('scroll', triggerWhenOnPage);
                };
            }
            $('body').on('scroll', triggerWhenOnPage);
        </script>

        <!-- Store redirect for logged out user -->
        <script>
            var params = window.location.search.slice(1);
            if (params) {
                params = $.deparam(params);
                $.cookie('redirect_back', params.redirect_back, { expires: 1, path: '/' });
            }
        </script>
        <script>
          $('#copyright-date').html(new Date().getFullYear());
        </script>
    </body>
</html>
